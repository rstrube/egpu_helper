#!/bin/sh

sudo cp -v xorg.conf.egpu /etc/X11/
sudo chmod 644 /etc/X11/xorg.conf.egpu

sudo cp -v egpu-helper /usr/local/bin/
sudo chmod 755 /usr/local/bin/egpu-helper
sudo cp -v egpu-delay.service /etc/systemd/system/

