# egpu-helper

Some scripts and a systemd service to assist in getting eGPUs working under Linux.
**Note: currently some of the PCI ID values are specific to my personal setup, so adjustments will be necessary to use these scripts and tools on your environment**